create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.2 -module_name fifocore3
set_property -dict {
CONFIG.Fifo_Implementation {Common_Clock_Builtin_FIFO}
CONFIG.Input_Depth {512}
CONFIG.Output_Depth {512}
CONFIG.Input_Data_Width {8}
CONFIG.Output_Data_Width {8}
CONFIG.Reset_Type {Asynchronous_Reset}
CONFIG.Use_Dout_Reset {true}
} [get_ips fifocore3]
generate_target {instantiation_template} [get_files fifocore3.xci]
generate_target all [get_files  fifocore3.xci]
export_ip_user_files -of_objects [get_files fifocore3.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] fifocore3.xci]
#CONFIG.Data_Count_Width {5}
#CONFIG.Write_Data_Count_Width {5}
#CONFIG.Read_Data_Count_Width {5}
#CONFIG.Full_Threshold_Assert_Value {30}
#CONFIG.Full_Threshold_Negate_Value {29}
