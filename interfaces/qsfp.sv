interface ifqsfp();
// wire tx_disable; wire los; wire tx_p; wire rx_p; wire tx_n; wire rx_n;
 wire [3:0] tx_p,tx_n;
 wire [3:0] rx_p,rx_n;
 wire mod_sel;
 wire rst;
 wire mod_prs;
 wire intp;
 wire lpmode;
 modport cfg(
	 output tx_p,tx_n
	 ,input rx_p,rx_n
	 );
 modport hw(
	 input tx_p,tx_n
	 ,output rx_p,rx_n
	 );
	 
endinterface
