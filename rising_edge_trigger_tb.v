`timescale 1ns / 1ns

module rising_edge_trigger_tb;

reg clk;
integer cc;
initial begin
	if ($test$plusargs("vcd")) begin
		$dumpfile("rising_edge_trigger.vcd");
		$dumpvars(5,rising_edge_trigger_tb);
	end
	for (cc=0; cc<200; cc=cc+1) begin
		clk=0; #5;
		clk=1; #5;
	end
end

reg pin=0;
integer ci;
always @(posedge clk) begin
	ci = cc % 100;
	if (ci < 10) pin <= 0;
	else if (ci < 30) pin <= $random;
	else if (ci < 70) pin <= 1;
	else if (ci < 80) pin <= $random;
	else              pin <= 0;
end

wire trigger;
rising_edge_trigger #(.cycles(6)) dut(.clk(clk), .pin(pin), .trigger(trigger));

endmodule
