`timescale 1ns / 1ns
// Dual port memory with independent clocks, port B is read-only
// Altera and Xilinx synthesis tools successfully "find" this as block memory
module dpram_count(
	clka, clkb,
	addra, douta, dina, wena,
	addrb, doutb,
	rst, stop
);
parameter aw=8;
parameter dw=8;
parameter sz=(32'b1<<aw)-1;
	input clka, clkb, wena, rst, stop;
	input [aw-1:0] addra, addrb;
	//input [dw-1:0] dina;
	input dina;  // used for counter, dina = 1'b0 or 1'b1
	output [dw-1:0] douta, doutb;

reg [dw-1:0] mem[sz:0];
reg [aw-1:0] ala=0, alb=0;

// In principle this should work OK for synthesis, but
// there seems to be a bug in the Xilinx synthesizer
// triggered when k briefly becomes sz+1.
integer k=0;
`ifdef SIMULATE
initial
begin
	for (k=0;k<sz+1;k=k+1)
	begin
		mem[k] = 0;
	end
end
`endif

assign douta = mem[ala];
assign doutb = mem[alb];

reg [aw:0] kk=0;
wire full;
assign full = kk[aw];
reg rst_d=0;
reg reseted=0;
always @(posedge clka) begin
	rst_d <= rst;
	reseted <= (rst & ~rst_d) ? 1'b1 : (full) ? 1'b0 : reseted;
	kk <= rst ? 0 : (~full) ? (kk + 1) : kk;
end

always @(posedge clka) begin
	if (reseted) begin
		mem[kk] = 0;
	end
	else begin
		ala <= addra;
		mem[addra] <= (~stop&wena) ? (mem[addra]+dina) : mem[addra];
	end
end
always @(posedge clkb) begin
	alb <= addrb;
end

endmodule
