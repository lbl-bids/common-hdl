module uartlb #(parameter UARTDWIDTH=8
,parameter LBWIDTH=64
)(input clk
,input reset
,input [UARTDWIDTH-1:0] fromuartdata
,input fromuartvalid
,output [LBWIDTH-1:0] lbwcmd
,output lbwvalid

,output [UARTDWIDTH-1:0] touartdata
,input touartready
,output touartstart
,input [LBWIDTH-1:0] lbrcmd
,input lbrready
,output dbresetcmd
);
//wire reset;
localparam UARTPERCMD=LBWIDTH/UARTDWIDTH;
reg [LBWIDTH-1:0] lbwcmd_r=0;
reg [LBWIDTH-1:0] lbwcmdsr=0;
reg [5:0] uartinwcmd=0;
wire uartzero=fromuartvalid&(~|fromuartdata);
reg [5:0] ffcnt=0;
wire resetcmd=(ffcnt>=UARTPERCMD-1 && uartzero);
assign dbresetcmd=resetcmd;
reg fromuartvalid_d=0;
wire lbwvalid_w=fromuartvalid_d&uartinwcmd==UARTPERCMD;
reg lbwvalid_r=0;
always @(posedge clk) begin
	if (reset || resetcmd) begin
		lbwcmdsr<=0;
		uartinwcmd<=0;
		ffcnt<=0;
	end
	else begin
		if (fromuartvalid) begin
			//if (uartinwcmd==UARTPERCMD) begin
			if (uartinwcmd==0) begin
				lbwcmdsr<={{(LBWIDTH-UARTDWIDTH){1'b0}},fromuartdata};
				uartinwcmd<=1;
			end
			else begin
				lbwcmdsr<={lbwcmdsr[LBWIDTH-UARTDWIDTH-1:0],fromuartdata};
				uartinwcmd<=(uartinwcmd==UARTPERCMD)? 1 : (uartinwcmd+1'b1);
			end
			if (fromuartdata=={{UARTDWIDTH{1'b1}}}) begin
				ffcnt<=ffcnt+1;
			end
			else begin
				ffcnt<=0;
			end
		end
	end
	fromuartvalid_d<=fromuartvalid;
	lbwvalid_r<=lbwvalid_w;
	if (lbwvalid_w)
		lbwcmd_r<=lbwcmdsr;
end
assign lbwvalid=lbwvalid_r;
assign lbwcmd=lbwcmd_r;


reg [LBWIDTH-1:0] lbrcmdsr=0;
reg [LBWIDTH-1:0] lbrcmd_r=0;
reg [5:0] uartinrcmd=0;
reg cmdbusyr=0;
reg cmdbusyr_d=0;
reg bytebusy=0;
reg bytebusy_d=0;
reg [UARTDWIDTH-1:0] touartdata_r=0;

wire busy=cmdbusyr;
always @(posedge clk) begin
	if (lbrready & ~busy) begin
		lbrcmd_r<=lbrcmd;
		uartinrcmd<=UARTPERCMD;
		cmdbusyr<=1'b1;
	end
	else begin
		if (touartready) begin
			case (uartinrcmd)
				UARTPERCMD: begin
					uartinrcmd<=uartinrcmd-1'b1;
					bytebusy<=1'b1;
					touartdata_r<=lbrcmd_r[LBWIDTH-1:LBWIDTH-UARTDWIDTH];
					lbrcmdsr<={lbrcmd_r[LBWIDTH-UARTDWIDTH-1:0],{UARTDWIDTH{1'b0}}};
				end
				0: begin
					cmdbusyr<=1'b0;
				end
				default: begin
					if (~bytebusy) begin
						uartinrcmd<=uartinrcmd-1'b1;
						bytebusy<=1'b1;
						touartdata_r<=lbrcmdsr[LBWIDTH-1:LBWIDTH-UARTDWIDTH];
						lbrcmdsr<={lbrcmdsr[LBWIDTH-UARTDWIDTH-1:0],{UARTDWIDTH{1'b0}}};
					end
				end
			endcase

			/*			if (uartinrcmd==UARTPERCMD-1) begin
		end
		else
			if (|uartinrcmd) begin
				if (~bytebusy) begin
					uartinrcmd<=uartinrcmd-1'b1;
					bytebusy<=1'b1;
					touartdata_r<=lbrcmdsr[LBWIDTH-1:LBWIDTH-UARTDWIDTH-1];;
					lbrcmdsr<={lbrcmdsr[LBWIDTH-UARTDWIDTH-1:0],{UARTDWIDTH{1'b0}}};
				end
			end
			else begin
				cmdbusyr<=1'b0;
			end
			*/
	   end
	   else begin
		   bytebusy<=1'b0;
	   end
   end
   cmdbusyr_d<=cmdbusyr;
   bytebusy_d<=bytebusy;
end
assign touartstart=(cmdbusyr & bytebusy&~bytebusy_d);
assign touartdata=touartdata_r;

endmodule
