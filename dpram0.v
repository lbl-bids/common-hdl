`timescale 1ns / 1ns
module dpram0 #(parameter AW=8
,parameter DW=8
,parameter BUFIN=0
,parameter BUFOUT=1 // BUFOUT need to be 1 to be recognized as BlockRAM
,parameter SIM=0
)(input clka
,input clkb
,input wena
,input renb
,input reset
,input [AW-1:0] addra
,input [AW-1:0] addrb
,input [DW-1:0] dina
,output [DW-1:0] douta
,output [DW-1:0] doutb
);
localparam SZ=(32'b1<<AW)-1;
reg [DW-1:0] mem[SZ:0];

integer k=0;
initial
begin
	for (k=0;k<SZ+1;k=k+1)
	begin
		mem[k]=0;
	end
end

wire wena_w;
wire [DW-1:0] dina_w;
wire [AW-1:0] addra_w;
wire [AW-1:0] addrb_w;
if (BUFIN) begin
	reg [AW-1:0] addra_r=0, addrb_r=0;
	reg wena_r=0;
	reg [DW-1:0] dina_r=0;
	always @(posedge clka) begin
		addra_r <= addra;
		wena_r<=wena;
		dina_r<=dina;
	end
	always @(posedge clkb) begin
		addrb_r <= addrb;
	end
	assign addra_w=addra_r;
	assign addrb_w=addrb_r;
	assign wena_w=wena_r;
	assign dina_w=dina_r;
end
else begin
	assign addra_w=addra;
	assign addrb_w=addrb;
	assign wena_w=wena;
	assign dina_w=dina;
end

genvar testi;
generate // begin :simtest
	if (SIM) begin
		wire [DW-1:0] mem0,mem1,mem2,mem3,mem4,mem5,mem6,mem7,mem8,mem9,mema,memb,memc,memd,meme,memf,mem10,mem11,mem12,mem13,mem14,mem15,mem16,mem17,mem18,mem19,mem1a,mem1b,mem1c,mem1d,mem1e,mem1f;
		wire [32*DW-1:0] memtest;
		for (testi=0;testi<32; testi=testi+1)
			assign memtest[(32-testi)*DW-1:(31-testi)*DW]=mem[testi];
		assign {mem0,mem1,mem2,mem3,mem4,mem5,mem6,mem7,mem8,mem9,mema,memb,memc,memd,meme,memf,mem10,mem11,mem12,mem13,mem14,mem15,mem16,mem17,mem18,mem19,mem1a,mem1b,mem1c,mem1d,mem1e,mem1f}=memtest;
	end
//end
endgenerate


wire [DW-1:0] douta_w = mem[addra_w]; // must buffer out to be recognized as Block RAM
wire [DW-1:0] doutb_w = mem[addrb_w];
reg [DW-1:0] douta_r=0;
reg [DW-1:0] doutb_r=0;
always @(posedge clka) begin
	if (wena_w) begin
		mem[addra_w]<=dina_w;
	end
	douta_r <= douta_w;//mem[addra_w]; // must buffer out to be recognized as Block RAM
end
always @(posedge clkb) begin
	if (renb)
		doutb_r <= doutb_w;//mem[addrb_w];
end

assign douta = BUFOUT ? douta_r : douta_w;
assign doutb = BUFOUT ? doutb_r : doutb_w;
endmodule
