# Converts a raw ctrace memory dump (example implemention in ctrace_tb.v)
# into a .vcd (Value Change Dump) file suited for viewing with GTKWave.
import sys

def tobin(x, count=8):
	# Integer to binary; count is number of bits
	# Credit to W.J. van der Laan in http://code.activestate.com/recipes/219300/
	return map(lambda y: (x >> y) & 1, range(count-1, -1, -1))

def vcd_header(dw, first):
	# 50 MHz and ns time step means multiply integer time count by 20
	print "$date November 11, 2009. $end"
	print "$version c2vcd $end"
	print "$timescale 1ns $end"
	print "$scope module logic $end"
	for ix in range(dw):
		print "$var wire 1 %s bit%2.2d $end" % (chr(65+ix), ix)
	print "$upscope $end"
	print "$enddefinitions $end"
	print "$dumpvars"
	for ix in range(dw):
		print "b%d %s" % (first[ix], chr(65+ix))
	print "$end"

t = 0
old_pc = -1
with open(sys.argv[1],'r') as f:
	for l in f.read().split('\n'):
		if not l:
			break
		a = l.split()
		if a[0] == "#":
			mtime = 1 << int(a[1])
			dw = int(a[2])
			old_vbin = ["" for ix in range(dw)]
			#print "setup",mtime,dw
			continue
		pc = int(a[0])
		if pc != old_pc + 1:
			print "out of order"
		old_pc = pc
		dt = int(a[1])
		if dt == 0:
			dt = mtime
		t = t + dt
		v = int(a[2],16)
		vbin = tobin(v, count=dw)
		if pc == 0:
			vcd_header(dw,vbin)
		else:
			for ix in range(dw):
				if vbin[ix] != old_vbin[ix]:
					print "b%d %s" % (vbin[ix], chr(65+ix))
		print "#%d"%(t*20)
		old_vbin = vbin
