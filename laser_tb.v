`timescale 1ns / 1ns
`include "constants.vams"

module laser_tb;

reg clk;
integer cc;
initial begin
	if ($test$plusargs("vcd")) begin
		$dumpfile("laser.vcd");
		$dumpvars(5,laser_tb);
	end
	for (cc=0; cc<3600; cc=cc+1) begin
		clk=0; #5;
		clk=1; #5;
	end
end

// Local bus
reg [31:0] lb_data=0;
reg [6:0] lb_addr=0;
reg lb_write=0;
reg [1:0] laser_ch_sel=0;
reg [7:0] laser_samp_per=2;

reg signed [15:0] adc1r=0, adc2r=0, adc3r=0, adc4r=0, outk=0, adcxr=0;
reg signed [17:0] cosa=0, sina=0, cosb=0, sinb=0;
wire rf_permit1=1, rf_permit2=1, permit2_mask=0;

reg signed [17:0] cosx=0, sinx=0;
reg signed [15:0] adc_test=0;
reg [2:0] theta=0;
reg [15:0] amp=21600;
real pha=0.5;
always @(posedge clk) begin
	theta <= theta==6 ? 0 : theta+1;
	cosx = 131000*$cos(theta*2.0*`M_PI/7.0);  cosa <= cosx;
	sinx = 131000*$sin(theta*2.0*`M_PI/7.0);  sina <= sinx;
	adc_test = amp*$sin(theta*2.0*`M_PI/7.0+pha);  adc1r <= adc_test;
	if (cc==400) amp=32400;
	if (cc==800) pha=1.5;
	if (cc>1200) pha=1.5+(cc-1200)*0.02;
end

initial begin
	#1;
	//timing.laser_samp_per=2;
	timing.laser_cnt=1;
	laser.pll_thresh=30000;
end

// Timing module
wire sample1, sample_wave, sample_decay, sample_laser, rf_master, reflect_fault;
wire pulse_trig;
wire rf_on, decay_trig, fault_trig;
atiming timing(.clk(clk),
	.sample1(sample1),
	.sample_wave(sample_wave),
	.sample_decay(sample_decay),
	.sample_laser(sample_laser),
	.rf_permit(rf_permit1&(rf_permit2|permit2_mask)),
	.reflect_fault(reflect_fault),
	.rf_on(rf_on),
	.rf_master(rf_master),
	.pulse_trig(pulse_trig),
	.decay_trig(decay_trig),
	.fault_trig(fault_trig),
	.lb_data(lb_data),
	.lb_write(lb_write),
	.lb_addr(lb_addr)
);

// Processing chain front end, results are used for
//   waveform monitoring
//   decay waveform acquisition
//   laser frequency measurement and feedback
//   reflected power trip
wire [35:0] sr_out;
wire sr_val;
cim_12 #(.dw(36)) cim(
	.clk(clk),
	.adca(adc1r), .adcb(adc2r), .adcc(adc3r), .adcd(adc4r),
	.outm(outk), .adcx(adcxr),
	.cosa(cosa), .sina(sina), .cosb(cosb), .sinb(sinb),
	.sample(sample1),
	.sr_out(sr_out), .sr_val(sr_val)
);

reg slow_op=0, slow_snap=0;
reg [7:0] slow_in=10;
always @(posedge clk) begin
	slow_op <= cc%10==3;
	slow_snap <= cc%1000==103;
	if (slow_op) slow_in <= slow_in+3;
end

//`define LQGR
`ifndef LQGR
integer ix;
initial for (ix=0; ix<8; ix=ix+1) laser.piloop.rfile.mem[ix]=(ix+26)*(ix+26)*65536*4;
`endif

// Laser frequency measurement and feedback
wire [15:0] mdac_val;
wire [2:0] mdac_addr;
wire [2:0] mdac_load;
wire mdac_trig, mdac_busy;
laser #(.dw(36)) laser(.clk(clk), .s0(sr_out), .svalid(sr_val&sample_laser),
	.ch_sel(laser_ch_sel), .buf_sync(1'b0),
	.lb_data(lb_data), .lb_addr(lb_addr), .lb_write(lb_write),
	.mdac_val(mdac_val), .mdac_addr(mdac_addr), .mdac_load(mdac_load),
	.mdac_trig(mdac_trig), .mdac_busy(mdac_busy),
	.slow_op(slow_op), .slow_snap(slow_snap), .slow_in(slow_in)
);

endmodule
