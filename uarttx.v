module uarttx #(parameter DWIDTH=8
,parameter NSTOP=1)(input clk
,input [15:0] bauddiv2
,input rst
,output txline
,input [DWIDTH-1:0] data
,input start
,output busy
,output ready
,output error
,output [1:0] txstate
,output [1:0] txnext
,output [5:0] txtxcnt
,output txbaudclk
,output [15:0] txbaudcnt
,output txstop
,output startfromtx
,output reg txline_r=0
);

reg [15:0] baudcnt=0;
wire baudcnt0=~|baudcnt;
reg [1:0] baudclk=0;
reg [1:0] baudclk_d=0;
wire stop;
reg txclk=0;
always@(posedge clk) begin
	if (txclk) begin
		baudcnt<= baudcnt0 ? bauddiv2-1 : baudcnt-1;
		if (baudcnt0) begin
			baudclk<=baudclk+1'b1;
		end
	end
	else begin
		baudcnt<=bauddiv2-1;
		baudclk<=0;
	end
	baudclk_d<=baudclk;
	if (start)  begin
		txclk<=1;
	end
	else begin
		if (stop) begin
			txclk<=0;
		end
	end
end
wire baudclkrising=baudclk[0]&~baudclk_d[0];
wire baudclkfaling=~baudclk[0]&baudclk_d[0];
wire newbaudperiod=baudclk[1]^baudclk_d[1];

reg [DWIDTH-1:0] data_r=0;
reg error_r=0;
always @(posedge clk)  begin
	if (start) begin
			data_r<=data;
	end
end

localparam IDLE=2'b00;
localparam START=2'b01;
localparam DATA=2'b10;
localparam STOP=2'b11;
reg [1:0] state=IDLE;
reg [1:0] next=IDLE;
assign stop=(state==STOP && next==IDLE);
always @ (posedge clk) begin
	if (rst) begin
		state <= IDLE;
	end
	else begin
		state <= next;
	end
end
localparam NSTART=1;
reg [5:0] txcnt=0;
always @(*) begin
	case (state)
		IDLE: begin
				next= start ? START : IDLE;
		end
		START: begin
				next= (txcnt==NSTART) ? DATA : START;
		end
		DATA: begin
			next = (txcnt==(DWIDTH+NSTART)) ? STOP : DATA;
		end
		STOP: begin
			next = (txcnt==(DWIDTH+NSTART+NSTOP) ) ? IDLE : STOP; 
		end
	endcase
end
reg [15:0] stopcnt=0;
reg [DWIDTH-1:0] datasr=0;
always @(posedge clk) begin
	if (rst) begin
		txline_r<=1'b1;
		stopcnt<=0;
	end
	else begin
		case (next)
			IDLE:begin
				txline_r<=1'b1;
				txcnt<=0;
				stopcnt<=0;
			end
			START:begin
				txline_r<=1'b0;
				datasr<=data_r;
				if (baudclkfaling) begin
					txcnt<=txcnt+1;
				end
			end
			DATA: begin
				txline_r<=datasr[0];
				if (baudclkfaling) begin
					txcnt<=txcnt+1;
					datasr<={1'b0,datasr[DWIDTH-1:1]};
				end
			end
			STOP: begin
				txline_r<=1'b1;
				stopcnt<=stopcnt+1;
				if (stopcnt==bauddiv2+bauddiv2-2) begin
					txcnt<=txcnt+1;
				end
			end
		endcase
	end
end
assign ready=(next==IDLE);
assign busy=~ready;
assign error=error_r;
assign txline=txline_r;
assign txstate=state;
assign txtxcnt=txcnt;
assign txnext=next;
assign txbaudclk=baudclk;
assign txstop=stop;
assign startfromtx=start;
assign txbaudcnt=baudcnt;
endmodule
