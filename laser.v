`timescale 1ns / 1ns

module laser(
	input clk,
	input [dw-1:0] s0,  // stream of double-integrated data
	input svalid,
	input [1:0] ch_sel,  // where in time to start looking at stream
	input excite_sync,      // can be used to synchronize test stimulus

	// Local bus
/*
	input [31:0] lb_data,
	input [6:0] lb_addr,
	input lb_write,
*/

	// Output
/*
	output [15:0] mdac_val,  // destination: ad56x4_driver3
	output [2:0] mdac_addr,
	output [2:0] mdac_load,
	output mdac_trig,
	input mdac_busy,
*/
	output trace_w,
	output trace_b

	// Slow shift register for host
/*
	input slow_op,
	input slow_snap,
	input [7:0] slow_in,
	output [7:0] slow_out
*/
	,input [15:0] pll_thresh

	,output [2:0] a_sets
	,input [21:0] v_sets

    ,input [0:0] laser_reverse
    ,input [0:0] laser3_reverse2
    ,input [0:0] laser3_use_static1
    ,input [0:0] laser3_use_static2
    ,input [1:0] laser3_excite_per
    ,input [0:0] laser3_excite1
    ,input [0:0] laser3_excite2
    ,input [15:0] laser3_excite_v

	,output [15:0] dac1_data
	,output dac1_gate
	,output [15:0] dac2_data
	,output dac2_gate
	,output dac_trig
	,input slow_reset


,output signed [15:0] laser_freq  // coarse info for host before lock
,output [15:0] laser_amp
,output [15:0] ss_status
,input [2:0] rfile_haddr
,output [31:0] rfile_hdata
);

`include "cordicg.vh"  // sets parameter cordic_delay for pipeline config
parameter dw=36;   // must match width of s0 input

// Host-settable registers
/*`include "regmap_app.vh"
`REG_pll_thresh
*/
// Universal definition; note: old and new are msb numbers, not bit widths.
`define SAT(x,old,new) ((~|x[old:new] | &x[old:new]) ? x[new:0] : {x[old],{new{~x[old]}}})

// Two stages of differentiator
// XXX kind of stupid to do this before channel selection; no resource
// penalty on Xilinx, though.
wire valid2;
wire [dw-1:0] d2;
doublediff #(.dw(dw), .dsr_len(12)) diff(.clk(clk),
	.d_in(s0), .g_in(svalid), .d_out(d2), .g_out(valid2));

// Select bits based on known (ha!) static timing of 28 cycles per sample
// Buffer once so we can get X and Y in the same cycle
wire signed [18:0] d2s=d2[29:11];
reg signed [17:0] d3=0, d4=0;
reg v3=0, v4=0;
wire signed [16:0] d2ss = `SAT(d2s,18,16);
always @(posedge clk) begin
	d3 <= {d2ss[16],d2ss};  v3 <= valid2;
	d4 <= d3;  v4 <= v3;
end

// Select first channel of laser IQ pair
reg [3:0] chan_cnt=0;
always @(posedge clk) chan_cnt <= v4 ? (chan_cnt+1'b1) : 0;
wire v4_laser = v4 & (chan_cnt=={1'b0,ch_sel,1'b1});

// CORDIC instantiation
wire signed [17:0] ampout;
wire [18:0] phaseout;
cordicg #(.width(18), .def_op(1)) r2p(.clk(clk), .opin(2'b01),
	.xin(d4), .yin(d3), .phasein(19'b0),
	.xout(ampout), .phaseout(phaseout)
);

// CORDIC pipeline tracking
reg [cordic_delay-1:0] sync_chain=0;
always @(posedge clk) sync_chain <= {sync_chain[cordic_delay-1:0],v4_laser};
wire v5 = sync_chain[cordic_delay-1];

// One more pipeline stage while we check amplitude
reg [15:0] amp_hold=0;
reg [16:0] phs_hold=0;
reg amp_ok=0;
reg v6=0;
always @(posedge clk) begin
	// assume ~ampout[17]
	if (v5) begin
		amp_hold <= ampout[15:0];
		phs_hold <= phaseout[18:2];
		amp_ok <= ampout[15:0] >= pll_thresh;
	end
	v6 <= v5;
end

freq freq(.clk(clk), .strobe(v6), .phase(phs_hold[16:15]), .freq(laser_freq));

// Run PI loop
wire signed [16:0] phase_mon;
wire strobe_dropped, bb_fault, bb_complete;
//`define LQGR
/*`ifdef LQGR
`define LOOP_MODULE lqg_loop1
`else
`define LOOP_MODULE piloop3
`endif
`LOOP_MODULE
Not sure still compatabile with LQGR, so piloop3 only for now
*/
piloop3 #(.win(17),.w(16)) piloop(.clk(clk),
	.sigin(phs_hold), .refin(17'b0), .excite_sync(excite_sync),
	.strobe_in(v6), .amp_ok(amp_ok),
	.rfile_haddr(rfile_haddr), .rfile_hdata(rfile_hdata),
/*
	.lb_data(lb_data), .lb_addr(lb_addr), .lb_write(lb_write),
	.mdac_out(mdac_val), .mdac_addr(mdac_addr), .mdac_load(mdac_load),
	.mdac_trig(mdac_trig), .mdac_busy(mdac_busy),
*/
	.strobe_dropped(strobe_dropped), .bb_fault(bb_fault), .bb_complete(bb_complete),
	.diffout(phase_mon), .trace_w(trace_w), .trace_b(trace_b)
	,.a_sets(a_sets),.v_sets(v_sets)
	,.dac1_data(dac1_data),.dac1_gate(dac1_gate)
	,.dac2_data(dac2_data),.dac2_gate(dac2_gate)
,.laser3_excite1(laser3_excite1)
,.laser3_excite2(laser3_excite2)
,.laser3_excite_per(laser3_excite_per)
,.laser3_excite_v(laser3_excite_v)
,.laser3_reverse2(laser3_reverse2)
,.laser3_use_static1(laser3_use_static1)
,.laser3_use_static2(laser3_use_static2)
,.laser_reverse(laser_reverse)
);

// capture single-cycle events for slow chain reporting
reg strobe_dropped_l=0, bb_fault_l=0;
always @(posedge clk) begin
	if (slow_reset| strobe_dropped) strobe_dropped_l <= strobe_dropped;
	if (slow_reset| bb_fault) bb_fault_l <= bb_fault;
end

assign ss_status= {13'b0,strobe_dropped_l,bb_fault_l,bb_complete};

`ifdef OLD
// Trap this value for slow host readout
reg signed [15:0] laser_dac=0;
always @(posedge clk) if (mdac_load[0]&(mdac_addr==3'd0)) laser_dac<=mdac_val;
`endif
/*
// Insert rfile_hdata into slow readout shift register.  Tricky.
// Only keep 16 bits of 8 rfile_hdata registers.
reg [3:0] bypass_cnt=0;
reg sr_first16=0;
reg [7:0] hbyte_hold=0;
assign rfile_haddr = bypass_cnt[3:1];
always @(posedge clk) begin
	if (slow_op & (slow_snap | sr_first16)) begin
		bypass_cnt <= slow_snap ? 0 : (bypass_cnt+1);
		if (slow_snap) sr_first16 <= 1;
		else if (&bypass_cnt) sr_first16 <= 0;
	end
	if (slow_op & ~bypass_cnt[0]) hbyte_hold <= rfile_hdata[23:16];
end
wire [7:0] rfile_mux = ~bypass_cnt[0] ? rfile_hdata[31:24] : hbyte_hold;

// Slow data shifted in needs to be delayed 16 cycles while we spit out rfile data
wire [7:0] rfile_bypass;
reg_delay #(.dw(8), .len(16)) bypass(.clk(clk), .gate(slow_op),
	.din(slow_in), .dout(rfile_bypass));
wire [7:0] rfile_slow_out = sr_first16 ? rfile_mux : rfile_bypass;
*/
// Our share of slow readout
// Adapt comments in common_hdl/freq.v for the APEX frequencies:
//  17-bit reference counter, laser_samp_per=2, means strobes arrive at
//  1300e6/7*11/20/2/laser_samp_per = 2.3214 MHz, and quarter-turn events
//  gives a raw frequency resolution of 4.4278 Hz updated at 17.711 Hz.
//  After the noise bit is dropped, the laser_freq register result has
//  units of 1300e6/7*11/20/44/2^17/2 = 8.8555 Hz.
assign laser_amp = amp_hold;
/*`define SLOW_SR_LEN 6*8
`define SLOW_SR_DATA { laser_amp, laser_freq, ss_status }
parameter sr_length = `SLOW_SR_LEN;
reg [sr_length-1:0] slow_read=0;
always @(posedge clk) if (slow_op) begin
	slow_read <= slow_snap ? `SLOW_SR_DATA : {slow_read[sr_length-9:0],rfile_slow_out};
end
assign slow_out = slow_read[sr_length-1:sr_length-8];
*/
// rfile_hdata code above more-or-less implements
// signed [15:0] rfile_0, rfile_1, rfile_2, rfile_3, rfile_4, rfile_5, rfile_6, rfile_7
// define SLOW_SR_DATA { rfile_0, rfile_1, rfile_2, rfile_3, rfile_4, rfile_5, rfile_6, rfile_7 }
// but uses far fewer resources

endmodule
