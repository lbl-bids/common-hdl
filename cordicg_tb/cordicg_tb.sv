module cordicg_tb(clk, opin, xin, yin, phasein, xout, yout, phaseout,error,gin,gout);
parameter OPIN=1'b0;
parameter WIDTH=18;
parameter NSTAGE=18;
parameter NORMALIZE=1;
localparam ZWIDTH=NSTAGE+1;
parameter BUFIN=1;
parameter GW=1;
input clk;   // timespec 8.33 ns
input [0:0] opin;  //  1 forces y to zero (rect to polar), 0 forces theta to zero (polar to rect)
input signed [WIDTH-1:0] xin;
input signed [WIDTH-1:0] yin;
input [ZWIDTH-1:0] phasein;
output signed [WIDTH-1:0] xout;
output signed [WIDTH-1:0] yout;
output [ZWIDTH-1:0] phaseout;
output error;
input [GW-1:0] gin;
output [GW-1:0] gout;

cordicg #(.BUFIN(BUFIN),.NORMALIZE(NORMALIZE),.WIDTH(WIDTH),.NSTAGE(NSTAGE),.GW(GW),.OPIN(OPIN))
cordicg2(.*);
cordicg #(.BUFIN(BUFIN),.NORMALIZE(NORMALIZE),.WIDTH(WIDTH),.NSTAGE(NSTAGE),.GW(GW),.OPIN(~OPIN))
cordicg21(.*);
cordicg1 #(.BUFIN(BUFIN),.NORMALIZE(NORMALIZE),.WIDTH(WIDTH),.NSTAGE(NSTAGE),.GW(GW))
cordicg1(.clk,.opin,.xin,.yin,.phasein,.gin,.xout(),.yout(),.phaseout(),.error(),.gout());
//cordicg dut(.clk,.opin,.xin,.yin,.phasein,.xout,.yout,.phaseout,.error,.gin,.gout);
endmodule
