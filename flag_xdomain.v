`timescale 1ns / 1ns
module flag_xdomain(
	input clk1,
	input flagin_clk1,
	input clk2,
	output flagout_clk2
);

reg flagtoggle_clk1=0;
(* ASYNC_REG = "TRUE" , KEEP="TRUE"*) reg [2:0] sync1_clk2=0;
always @(posedge clk1) begin
	if (flagin_clk1) begin
		flagtoggle_clk1 <= ~flagtoggle_clk1;
	end
end
always @(posedge clk2) begin
	sync1_clk2 <= {sync1_clk2[1:0],flagtoggle_clk1};
end

assign flagout_clk2 = sync1_clk2[2]^sync1_clk2[1];
endmodule
