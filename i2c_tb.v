`timescale 1ns / 1ns
module i2c_tb();
reg sysclk=0;
integer cc=0;
initial begin
	$dumpfile("i2c.vcd");
	$dumpvars(17,i2c_tb);
	for (cc=0; cc<15000; cc=cc+1) begin
//while (1) begin
//		cc=cc+1;

		sysclk=0; #2.5;
		sysclk=1; #2.5;
	end
	$finish();
end

reg [31:0] clkcnt=0;
always @(posedge sysclk) begin
	clkcnt<=clkcnt+1;
end
wire start=~|clkcnt[11:0];
wire stop=&clkcnt[6:0];
wire sda1,sda2;
wire scl1,scl2;

/*i2cmaster #(.NACK(4))
i2cmasterrx(.clk(sysclk),.start(start),.datatx(8'hc5),.SCL(scl1),.SDA(sda1),.tx1rx0(1'b0));
i2cslave i2cslavetx(.clk(sysclk),.SCL(scl1),.SDA(sda1),.rst(1'b0),.tx1rx0(1'b1),.datatx(32'hdeadbeef));

i2cmaster #(.NACK(4))  i2cmastertx(.clk(sysclk),.start(start),.datatx(32'hccf5dd33),.SCL(scl2),.SDA(sda2),.tx1rx0(1'b1));
i2cslave i2cslaverx(.clk(sysclk),.SCL(scl2),.SDA(sda2),.rst(1'b0),.tx1rx0(1'b0),.datatx(8'hde));
*/
wire slaveack1valid;
wire [7:0] slaveack1;
wire [23:0] slavereg=clkcnt[30:7];//24'hadbeef;
reg slavetx1rx0=0;
wire slaverxvalid;
wire [7:0] slavedatarx;
wire r1w0=clkcnt>3000;
i2cmaster #(.MAXNACK(4))
i2cmaster(.clk(sysclk),.start(start),.datatx({7'h62,r1w0,clkcnt[31:8]}),.scl(scl1),.nack(4'h4),.clk4ratio(32'd10),.stopbit(1'b1)
,.sdarx(sdarx)
,.sdatx(sdatx)
,.sdaasrx(sdaasrx)
);

assign sda1 = sdaasrx ? 1'bz : sdatx;
assign sdarx = sdaasrx ? sda1 : 1'bz;
IOBUF sdaiobuf (.IO(sda1),.I(sdatx),.O(sdarx),.T(sdaasrx));
wire slavesdatx,slavesdarx,slavesdaasrx;
IOBUF slavesdaiobuf (.IO(sda1),.I(slavesdatx),.O(slavesdarx),.T(slavesdaasrx));
//assign sda1= sdaasrx ? 1'bz : sdatx;
//assign sdarx = sdaasrx ? sda1 : 1'bz;
i2cslave i2cslave(.clk(sysclk),.SCL(scl1)
//,.SDA(sda1)
,.slavesdatx(slavesdatx)
,.slavesdarx(slavesdarx|(sdaasrx&slavesdaasrx)|(~sdaasrx&~slavesdaasrx&(sdatx^slavesdatx)))
,.slavesdaasrx(slavesdaasrx)
,.rst(1'b0),.datatx({8'haf}),.ack1(slaveack1),.ack1valid(slaveack1valid),.datarx(slavedatarx),.rxvalid(slaverxvalid));
/*always @(posedge sysclk) begin
	if (slaveack1valid)
		slavetx1rx0<=slaveack1[0];
	if (slaveack1[0]==0)
		if (slaverxvalid)
			slavereg<=slavedatarx[23:0];
end
*/

endmodule
