//interface localbus#(parameter LBCWIDTH=8,parameter LBAWIDTH=24,parameter LBDWIDTH=32)();

localparam LBWIDTH=LBCWIDTH+LBAWIDTH+LBDWIDTH;
wire lbclk;
wire [LBWIDTH-1:0] lbwcmd;
wire lbwvalid;
wire [LBWIDTH-1:0] lbrcmd;
wire lbrready;

reg [LBCWIDTH-1:0] lbwctrl=0;
reg [LBAWIDTH-1:0] lbwaddr=0;
reg [LBDWIDTH-1:0] lbwdata=0;
reg [LBCWIDTH-1:0] lbrctrl=0;
reg [LBAWIDTH-1:0] lbraddr=0;
reg [LBDWIDTH-1:0] lbrdata=0;
reg [LBWIDTH-1:0] lbrcmd_r=0;
reg lbwrite=0;
reg lbread=0;
reg lbrready_r=0;
reg lbwvalid_r=0;

reg lbrready_r1=0;
reg lbrready_r2=0;

always @(posedge lbclk) begin
	{lbwctrl,lbwaddr,lbwdata}<=lbwcmd;
	lbwvalid_r<=lbwvalid;
	lbrready_r<=lbwvalid;
	lbwrite<=lbwcmd[63:56]==WRITECMD; // 1 for uart, 0 for udp
	lbread<=lbwcmd[63:56]==READCMD;  // 0 for uart, 0x10 for udp
	lbrready_r2<=lbrready_r1;
end
`include "lbwrite.vh"
always @(posedge lbclk) begin
	case (lbwctrl)
		WRITECMD: begin
			lbrctrl<=lbwctrl;
			lbraddr<=lbwaddr;
			lbrdata<=lbwdata;
			lbrready_r1<=lbrready_r;
		end
		READCMD: begin
			lbrctrl<=lbwctrl;
			lbraddr<=lbwaddr;
			lbrready_r1<=lbrready_r;
			casex (lbwaddr)
				`include "lbread.vh"
				default: lbrdata<= 32'hdeadbeef;
			endcase
		end
		default: begin
			lbrctrl<=lbwctrl;
			lbraddr<=lbwaddr;
			lbrdata<=lbwdata;
			lbrready_r1<=lbrready_r;
		end
	endcase
	lbrcmd_r<={lbrctrl,lbraddr,lbrdata};
end
assign lbrcmd=lbrcmd_r;//{lbrctrl,lbraddr,lbrdata};
assign lbrready= lbrready_r2;

//endinterface
