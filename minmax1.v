`timescale 1ns / 1ns

module minmax1(clk,xin,reset,xmin,xmax);
parameter WIDTH=14;
parameter NCHAN=8;
input clk;
input [NCHAN*WIDTH-1:0] xin;
input reset;
output [NCHAN*WIDTH-1:0] xmin;
output [NCHAN*WIDTH-1:0] xmax;
genvar iadc;
generate for (iadc=0;iadc<NCHAN;iadc=iadc+1) begin: adcminmax
	wire signed [WIDTH-1:0] xv=$signed(xin[(iadc+1)*WIDTH-1:iadc*WIDTH]);
	minmax #(.width(WIDTH)) minmax(.clk(clk), .xin(xv), .reset(reset), .xmin(xmin[(iadc+1)*WIDTH-1:iadc*WIDTH]), .xmax(xmax[(iadc+1)*WIDTH-1:iadc*WIDTH]));
end
endgenerate
endmodule
