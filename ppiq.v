module ppiq
#(parameter NORMALIZE=1
,parameter WIDTH=16
,parameter LOWIDTH=20
,parameter SCALESHIFT=14
)
(input clk
,input signed [WIDTH-1:0] vin
,input signed [LOWIDTH-1:0] cos
,input signed [LOWIDTH-1:0] sin
,output signed [WIDTH-1:0] xout
,output signed [WIDTH-1:0] yout
,input signed [WIDTH-1:0] scale
);
reg [WIDTH-1:0] voutsinr=0;
reg [WIDTH-1:0] voutcosr=0;
wire [WIDTH-1:0] voutsinw;
wire [WIDTH-1:0] voutcosw;
ppiq1 #(.NORMALIZE(NORMALIZE),.WIDTH(WIDTH),.LOWIDTH(LOWIDTH),.REVERSE(0),.SCALESHIFT(SCALESHIFT))
ppiqx(.clk(clk),.cossin(sin),.scale(scale),.vin(vin),.vout(voutsinw));
ppiq1 #(.NORMALIZE(NORMALIZE),.WIDTH(WIDTH),.LOWIDTH(LOWIDTH),.REVERSE(0),.SCALESHIFT(SCALESHIFT))
ppiqy(.clk(clk),.cossin(cos),.scale(scale),.vin(vin),.vout(voutcosw));
always @(posedge clk) begin
	voutsinr<=voutsinw;
	voutcosr<=-voutcosw;
end
assign xout=voutsinr;
assign yout=voutcosr;
endmodule
