# Function to define a bus port
proc busport {portname default_value master_presence master_direction master_width slave_presence slave_direction slave_width} {
    incr default_value 0
    incr master_width 0
    incr slave_width 0
    ipx::add_bus_abstraction_port ${portname} [ipx::current_busabs]
    set_property default_value $default_value [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
    set_property master_presence $master_presence [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
    set_property master_direction $master_direction [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
    set_property master_width $master_width [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
    set_property slave_presence $slave_presence [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
    set_property slave_direction $slave_direction [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
    set_property slave_width $slave_width [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
}

# Function to define a port pin
proc portpin {portname physical_name} {
    ipx::add_port_map ${portname} [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
    set_property physical_name ${physical_name} [ipx::get_port_maps ${portname} -of_objects [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]]
}

# Function to print usage information
proc print_usage {} {
    puts "Usage: vivado -mode batch -source script.tcl -rootpath <rootpath>"
}

# Default values
set args_dict {
    rootpath "../.."
}

# Function to parse command-line arguments into a dictionary
proc parse_args {args args_dict} {
    array set args_array $args_dict
    set len [llength $args]
    for {set i 0} {$i < $len} {incr i} {
        set key [lindex $args $i]
        if {[string match -* $key]} {
            incr i
            if {$i < $len} {
                set value [lindex $args $i]
                set key [string trimleft $key -]
                set args_array($key) $value
            } else {
                puts "Error: Missing value for $key"
                exit 1
            }
        } else {
            puts "Error: Unexpected argument $key"
            print_usage
            exit 1
        }
    }
    return [array get args_array]
}

# Main function
proc main {args} {
    # Default values
    set args_dict {
        rootpath "../.."
    }
    # Parse the arguments
    array set args_array [parse_args $args $args_dict]
	puts $args_array(rootpath)

    # Extract the rootpath from the dictionary
    set rootpath $args_array(rootpath)

    # Create project
    create_project iflocalbus ./vivado_project/iflocalbus -part xczu49dr-ffvf1760-2-e -force
    add_files -norecurse ${rootpath}/submodules/common-hdl/axi4_lb.v 
	
    # Package project
    ipx::package_project -root_dir ./vivado_project/iflocalbus -vendor user.org -library user -taxonomy /UserIP -import_files -set_current false -force
    ipx::unload_core ./vivado_project/iflocalbus/component.xml
    ipx::open_ipxact_file ./vivado_project/iflocalbus/component.xml
    ipx::create_abstraction_definition user user iflocalbus_rtl 1.0
    ipx::create_bus_definition user user iflocalbus 1.0
    set_property xml_file_name ./vivado_project/iflocalbus/iflocalbus_rtl.xml [ipx::current_busabs]
    set_property xml_file_name ./vivado_project/iflocalbus/iflocalbus.xml [ipx::current_busdef]
    set_property bus_type_vlnv user:user:iflocalbus:1.0 [ipx::current_busabs]
    ipx::save_abstraction_definition [ipx::current_busabs]
    ipx::save_bus_definition [ipx::current_busdef]
    set_property ip_repo_paths ./vivado_project/iflocalbus [current_project]
    update_ip_catalog

    # Define bus ports
    busport wren 0 required out 1 required in 1
    busport waddr 0 required out 32 required in 32
    busport wdata 0 required out 32 required in 32
    busport rden 0 required out 1 required in 1
    busport rdenlast 0 required out 1 required in 1
    busport rvalid 0 required in 1 required out 1
    busport rvalidlast 0 required in 1 required out 1
    busport raddr 0 required out 32 required in 32
    busport rdata 0 required in 32 required out 32
    busport clk 0 required out 1 required in 1
    busport aresetn 0 required out 1 required in 1

    ipx::save_bus_definition [ipx::current_busdef]
    set_property bus_type_vlnv user:user:iflocalbus:1.0 [ipx::current_busabs]
    ipx::save_abstraction_definition [ipx::current_busabs]
    update_ip_catalog -rebuild -repo_path ./vivado_project/iflocalbus
    ipx::add_bus_interface lb [ipx::current_core]

    set_property abstraction_type_vlnv user:user:iflocalbus_rtl:1.0 [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
    set_property bus_type_vlnv user:user:iflocalbus:1.0 [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
    set_property interface_mode master [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]

    # Define port pins
    portpin rdata lb_rdata
    portpin raddr lb_raddr
    portpin rden lb_rden
    portpin rdenlast lb_rdenlast
    portpin wdata lb_wdata
    portpin waddr lb_waddr
    portpin wren lb_wren
    portpin rvalid lb_rvalid
    portpin rvalidlast lb_rvalidlast
    portpin clk lb_clk
    portpin aresetn lb_aresetn

    # Additional settings
    set_property interface_mode master [ipx::get_bus_interfaces lb_clk -of_objects [ipx::current_core]]
    ipx::add_bus_parameter FREQ_HZ [ipx::get_bus_interfaces lb_clk -of_objects [ipx::current_core]]
    set_property value CLK_FREQ_HZ [ipx::get_bus_parameters FREQ_HZ -of_objects [ipx::get_bus_interfaces lb_clk -of_objects [ipx::current_core]]]

    ipx::create_xgui_files [ipx::current_core]
    ipx::update_checksums [ipx::current_core]
    ipx::check_integrity [ipx::current_core]
    ipx::save_core [ipx::current_core]
    set_property ip_repo_paths {./vivado_project/iflocalbus} [current_project]
    update_ip_catalog
    close_project
}

# Main script
if { [llength $argv] > 0 } {
    # Call the main function with the parsed arguments
    main {*}$argv
} else {
    print_usage
    exit 1
}

# g 
# g 
# g #wren 		0 				required 	out				 1 				required 		in 				1
# g #portname default_value master_presence master_direction master_width slave_presence slave_direction slave_width
# g proc busport {portname default_value master_presence master_direction master_width slave_presence slave_direction slave_width} {
# g puts $master_direction
# g incr $default_value 0
# g incr $master_width 0
# g incr $slave_width 0
# g ipx::add_bus_abstraction_port ${portname} [ipx::current_busabs]
# g set_property default_value $default_value [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
# g set_property master_presence $master_presence [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
# g set_property master_direction $master_direction [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
# g set_property master_width $master_width [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
# g set_property slave_presence $slave_presence [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
# g set_property slave_direction $slave_direction [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
# g set_property slave_width $slave_width [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
# g }
# g proc portpin {portname physical_name} {
# g ipx::add_port_map ${portname} [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
# g set_property physical_name ${physical_name} [ipx::get_port_maps ${portname} -of_objects [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]]
# g }
# g 
# g # todo: remove hardcoded submodule path
# g create_project vivado_project/iflocalbus iflocalbus -part xczu49dr-ffvf1760-2-e -force
# g add_files -norecurse ../../submodules/common-hdl/axi4_lb.v 
# g 
# g ipx::package_project -root_dir vivado_project/iflocalbus -vendor user.org -library user -taxonomy /UserIP -import_files -set_current false -force
# g ipx::unload_core vivado_project/iflocalbus/component.xml
# g #ipx::edit_ip_in_project -upgrade true -name tmp_edit_project -directory /home/ghuang/pynqtop/zcu216/rfsoctest/boards/zcu216/ip_repo /home/ghuang/pynqtop/zcu216/rfsoctest/boards/zcu216/ip_repo/component.xml
# g #update_compile_order -fileset sources_1
# g #current_project project_1
# g ipx::open_ipxact_file vivado_project/iflocalbus/component.xml
# g #ipx::merge_project_changes hdl_parameters [ipx::current_core]
# g ipx::create_abstraction_definition user user iflocalbus_rtl 1.0
# g ipx::create_bus_definition user user iflocalbus 1.0
# g set_property xml_file_name vivado_project/iflocalbus/iflocalbus_rtl.xml [ipx::current_busabs]
# g set_property xml_file_name vivado_project/iflocalbus/iflocalbus.xml [ipx::current_busdef]
# g set_property bus_type_vlnv user:user:iflocalbus:1.0 [ipx::current_busabs]
# g ipx::save_abstraction_definition [ipx::current_busabs]
# g ipx::save_bus_definition [ipx::current_busdef]
# g set_property  ip_repo_paths  vivado_project/iflocalbus [current_project]
# g update_ip_catalog
# g 
# g # {portname default_value master_presence master_direction master_width slave_presence slave_direction slave_width} {
# g # busport {portname default_value master_presence master_direction master_width slave_presence slave_direction slave_width} {
# g busport wren 0 required out 1 required in 1
# g busport waddr 0 required out 32 required in 32
# g busport wdata 0 required out 32 required in 32
# g busport rden 0 required out 1 required in 1
# g busport rdenlast 0 required out 1 required in 1
# g busport rvalid 0 required in 1 required out 1
# g busport rvalidlast 0 required in 1 required out 1
# g busport raddr 0 required out 32 required in 32
# g busport rdata 0 required in 32 required out 32
# g busport clk 0 required out 1 required in 1
# g busport aresetn 0 required out 1 required in 1
# g #}
# g ipx::save_bus_definition [ipx::current_busdef]
# g set_property bus_type_vlnv user:user:iflocalbus:1.0 [ipx::current_busabs]
# g ipx::save_abstraction_definition [ipx::current_busabs]
# g update_ip_catalog -rebuild -repo_path vivado_project/iflocalbus
# g ipx::add_bus_interface lb [ipx::current_core]
# g 
# g set_property abstraction_type_vlnv user:user:iflocalbus_rtl:1.0 [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
# g set_property bus_type_vlnv user:user:iflocalbus:1.0 [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
# g set_property interface_mode master [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
# g 
# g portpin rdata lb_rdata
# g portpin raddr lb_raddr
# g portpin rden lb_rden
# g portpin rdenlast lb_rdenlast
# g portpin wdata lb_wdata
# g portpin waddr lb_waddr
# g portpin wren lb_wren
# g portpin rvalid lb_rvalid
# g portpin rvalidlast lb_rvalidlast
# g portpin clk lb_clk
# g portpin aresetn lb_aresetn
# g 
# g 
# g 
# g #ipx::add_port_map rdata [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
# g #set_property physical_name lb_rdata [ipx::get_port_maps rdata -of_objects [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]]
# g #ipx::add_port_map waddr [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
# g #set_property physical_name lb_waddr [ipx::get_port_maps waddr -of_objects [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]]
# g #ipx::add_port_map wstrb [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
# g #set_property physical_name lb_wstrb [ipx::get_port_maps wstrb -of_objects [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]]
# g #ipx::add_port_map wdata [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
# g #set_property physical_name lb_wdata [ipx::get_port_maps wdata -of_objects [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]]
# g #ipx::add_port_map wren [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
# g #set_property physical_name lb_wvalid [ipx::get_port_maps wren -of_objects [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]]
# g #ipx::add_port_map raddr [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
# g #set_property physical_name lb_raddr [ipx::get_port_maps raddr -of_objects [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]]
# g #ipx::add_port_map clk [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
# g #set_property physical_name lb_clk [ipx::get_port_maps clk -of_objects [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]]
# g #ipx::add_port_map aresetn [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
# g #set_property physical_name lb_aresetn [ipx::get_port_maps aresetn -of_objects [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]]
# g 
# g set_property interface_mode master [ipx::get_bus_interfaces lb_clk -of_objects [ipx::current_core]]
# g #ipx::remove_port_map CLK [ipx::get_bus_interfaces lb_clk -of_objects [ipx::current_core]]
# g #ipx::add_bus_parameter FREQ_HZ [ipx::get_bus_interfaces lb_clk -of_objects [ipx::current_core]]
# g ipx::add_bus_parameter FREQ_HZ [ipx::get_bus_interfaces lb_clk -of_objects [ipx::current_core]]
# g set_property value CLK_FREQ_HZ [ipx::get_bus_parameters FREQ_HZ -of_objects [ipx::get_bus_interfaces lb_clk -of_objects [ipx::current_core]]]
# g 
# g #set_property core_revision 2 [ipx::current_core]
# g ipx::create_xgui_files [ipx::current_core]
# g ipx::update_checksums [ipx::current_core]
# g ipx::check_integrity [ipx::current_core]
# g ipx::save_core [ipx::current_core]
# g set_property  ip_repo_paths  {vivado_project/iflocalbus} [current_project]
# g update_ip_catalog
# g close_project
# g 
# g 
