module seqinit #(parameter INITWIDTH=27
,parameter INITLENGTH=4
,parameter RESULTWIDTH=1
,parameter INITCMDS=0)

(input clk
,input reset
,input runing
,output [INITWIDTH-1:0] cmd
//,input [RESULTWIDTH-1:0] resultwire
//,output [RESULTWIDTH-1:0] result
,output start
,output initdone
//,input [INITWIDTH-1:0] livecmd
//,input livestart
,output dbreset
,output [3:0] dbstate
,output [3:0] dbnext
);

function integer clog2;
    input integer value;
    begin
        value = value-1;
        for (clog2=0; value>0; clog2=clog2+1)
            value = value>>1;
    end
endfunction

reg [INITWIDTH-1:0] cmd_r=0;
reg [RESULTWIDTH-1:0] result_r=0;

assign dbreset=reset;
localparam INITLENWIDTH= clog2(INITLENGTH+1);
reg [INITLENWIDTH-1:0] initcnt=INITLENGTH;
localparam INITPREP=4'd0;
localparam INITSTART=4'd1;
localparam INITRUN=4'd2;
localparam INITNEXT=4'd3;
localparam INITDONE=4'd4;
reg done=0;
reg [3:0] state=INITDONE;
reg [3:0] next=INITDONE;
assign dbstate=state;
assign dbnext=next;
reg prep=0;
always @(posedge clk) begin
	if (reset)
		state<=INITPREP;
	else begin
		state<=next;
	end
end
always @(*) begin
	case (state)
		INITPREP: next = prep ? INITSTART : INITPREP;
		INITSTART: next = runing ? INITRUN : INITSTART;
		INITRUN: next = runing ? INITRUN : INITNEXT;
		INITNEXT: next= |initcnt ? INITPREP: INITDONE;
		INITDONE: next =INITDONE;
	endcase
end
reg [INITWIDTH-1:0] initcmd=0;
reg start_r=0;
assign {opr1w0_i,phyaddr_i,regaddr_i,datatx_i}=initcmd;
always @(posedge clk) begin
	//reset<=rst;
	if (reset) begin
		initcnt<=INITLENGTH;
		initcmd<=0;
		done<=1'b0;
		prep<=1'b0;
		start_r<=1'b0;
	end
	else begin
		case (next)
			INITPREP: begin
				initcmd<=INITCMDS[(initcnt-1)*INITWIDTH+:INITWIDTH];
				prep<=1'b1;
			end
			INITSTART: begin
				prep<=1'b0;
				start_r<=1'b1;
			end
			INITRUN: begin
				start_r<=1'b0;
			end

			INITNEXT: begin
				initcnt<=initcnt-1;
			end
			INITDONE: begin
//				initcmd<=livecmd;
//				start_r<=livestart;
				done<=1'b1;
			end
		endcase
	end
//	result_r<=resultwire;
end
assign start=start_r;
assign cmd=initcmd;
assign initdone=done;
//assign result=result_r;
endmodule
