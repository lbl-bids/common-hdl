module ppiq1
#(parameter NORMALIZE=1
,parameter WIDTH=16
,parameter LOWIDTH=20
,parameter REVERSE=0
,parameter SCALESHIFT=14
)
(input clk
,input signed [WIDTH-1:0] vin
,input signed [LOWIDTH-1:0] cossin
,output signed [WIDTH-1:0] vout
,input signed [WIDTH-1:0] scale
);

reg signed [WIDTH-1:0] vin_d1=0;
reg signed [WIDTH-1:0] vin_d2=0;
reg signed [LOWIDTH-1:0] cossin_d1=0;
reg signed [LOWIDTH-1:0] cossin_d2=0;
reg signed [LOWIDTH+WIDTH-1:0] m1=0;
reg signed [LOWIDTH+WIDTH-1:0] m2=0;
reg signed [LOWIDTH+WIDTH-1:0] d1=0;
reg signed [WIDTH-1:0] x1=0;
reg signed [WIDTH-1:0] s1=0;
reg signed [2*WIDTH-1:0] sm1=0;
always @(posedge clk) begin
	vin_d1<=vin;
	vin_d2<=vin_d1;
	cossin_d1<=cossin;
	cossin_d2<=cossin_d1;
	m1<=vin_d2*cossin_d1;
	m2<=vin_d1*cossin_d2;
	d1<=REVERSE ? m2-m1 : (m1-m2);
	x1<=d1>>>(LOWIDTH-1);
	sm1<=(x1*scale);
	s1<=sm1>>>SCALESHIFT;
end
assign vout=s1;
endmodule
