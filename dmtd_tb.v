//`include "constants.vams"
//`include "xc7vx485tffg1761pkg.vh"
`timescale 1ns / 1fs
module dmtd_tb(
);
reg sysclk=0;
integer cc=0;
initial begin
	$dumpfile("dmtd.vcd");
	$dumpvars(17,dmtd_tb);
	for (cc=0; cc<1500000; cc=cc+1) begin
//while (1) begin
//		cc=cc+1;
		sysclk=0; #2.5;
		sysclk=1; #2.5;
	end
	$finish();
end

reg [31:0] sysclkcnt=0;
always @(posedge sysclk) begin
	sysclkcnt<=sysclkcnt+1;
end

reg clkref=0;
initial begin
    forever #(8) clkref=~clkref;
end
reg [31:0] clkrefcnt=0;
always @(posedge clkref) begin
	clkrefcnt<=clkrefcnt+1;
end
localparam DELAY=0.8;
reg clkref_delay=0;
always @(*) begin
	clkref_delay = #(DELAY) clkref;
end

reg clkhelp=0;
initial begin
    //forever #(8.151) clkhelp=~clkhelp;
//    forever #(7.999912) clkhelp=~clkhelp;
	forever #(8.000488) clkhelp=~clkhelp;
end
reg [31:0] clkhelpcnt=0;
always @(posedge clkhelp) begin
	clkhelpcnt<=clkhelpcnt+1;
end
wire dmtdreset_w=sysclkcnt==100;
dmtd
dmtd (.clkdmtd(clkhelp)
,.clka(clkref)
,.clkb(clkref_delay)
,.rst(dmtdreset_w)
//,.navr(5'h3)
,.stableval(1)
,.offsetwidth(5'd3)
//,.refcnt(5'd14)
);
endmodule
