module dcrej(input [0:0] clk
,input signed [WIDTH-1:0] din
,output signed [WIDTH-1:0] dout
,input [0:0] reset
);
//`define SAT(x,old,new) ((~|x[old:new] | &x[old:new]) ? x[new:0] : {x[old],{new{~x[old]}}})
parameter WIDTH=16;
parameter WINTE=28;
reg signed [WIDTH-1:0] din_r=0;
reg signed [WIDTH+WINTE-1:0] inte=0;
reg signed [WIDTH:0] dout_r=0;
wire signed [WIDTH-1:0] dout_sat_w;
reg signed [WIDTH-1:0] dout_sat_r=0;
wire [WIDTH-1:0] inteoffset=(inte>>>WINTE);
always @(posedge clk) begin
	din_r<=din;
	inte <= reset ? 0 : inte + dout_r;
	dout_r <= reset ? din_r : (din_r - (inte>>>WINTE));
	dout_sat_r<=dout_sat_w;
end
sat #(.WIN(WIDTH+1),.WOUT(WIDTH)) sat (.din(dout_r),.dout(dout_sat_w));
assign dout=dout_sat_r;
//assign dout = `SAT(dout_r,WIDTH,WIDTH-1);
endmodule
