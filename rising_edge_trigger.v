`timescale 1ns / 1ns

// Fires with lower latency than could be achieved by a
// debounce followed by an edge detect.
module rising_edge_trigger(
	input clk,
	input pin,
	output trigger
);

parameter cycles=6;

reg state=0;
reg [cycles-1:0] sr=0;

always @(posedge clk) begin
	sr <= {sr[cycles-2:0],pin};
	if (sr[0]) state <= 1;
	if (~(|sr)) state <= 0;
end

assign trigger = sr[0] & ~state;
endmodule
