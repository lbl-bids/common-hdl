// Multiplexed-input frequency counter
// FPGA-friendly, there's no actual multiplexed clock:
//  multiplexing happens in the data path, requiring a small amount
//  of logic in each "unknown" clock domain

// Sensible choices for gw are 3 (supports f_unk/f_ref < 6.0) or
// 4 (supports f_unk/f_ref < 14.0).

// To get decent frequency resolution, the update rate is naturally pretty
// slow.  With default rw=24, NF=8, and a 50 MHz refclk, each channel
// updates every 2.7 seconds.  If the software wants to keep track of updates,
// it can peek at source_state:  the low order NA bits represent the channel
// currently acquiring, which will be the stalest result in memory.
// The upper cw (default 3) bits of source_state count macro-cycles of
// updating all NF frequency counters.
module freq_multi_count #(
	parameter NF=8,  // number of frequency counters
	parameter NA=3,  // number of address bits (ceil(log2(NF)))
	parameter gw=4,  // Gray counter width
	parameter cw=3,  // macro-cycle counter width
	parameter rw=24, // reference counter width
	parameter uw=28  // unknown counter width
) (
	// Input clocks
	input [NF-1:0] unk_clk,
	input refclk,
	// Result channel selection in refclk domain
	input [NA-1:0] addr,
	// Outputs in refclk domain
	output [NA+cw-1:0] source_state,
	output [uw-1:0] frequency
);

// One Gray code counter for each input clock
// http://en.wikipedia.org/wiki/Gray_code
reg [gw-1:0] gray1[0:NF-1];
initial for (jx=0; jx<NF; jx=jx+1) gray1[jx]=0;
genvar ix;
generate for (ix=0; ix<NF; ix=ix+1) begin
	// The following three expressions compute the next Gray code based on
	// the current Gray code.  Vivado 2016.1, at least, is capable of reducing
	// them to the desired four LUTs when gw==4.
	wire [gw-1:0] bin1 = gray1[ix] ^ {1'b0, bin1[gw-1:1]};  // Gray to binary
	wire [gw-1:0] bin2 = bin1 + 1;  // add one
	wire [gw-1:0] gray_next = bin2 ^ {1'b0, bin2[gw-1:1]};  // binary to Gray
	always @(posedge unk_clk[ix]) gray1[ix] <= gray_next;
end
endgenerate

// Transfer those Gray codes to the measurement clock domain
reg [gw-1:0] gray2[0:NF-1];
reg [gw-1:0] gray3=0, gray4=0;
integer jx;
initial for (jx=0; jx<NF; jx=jx+1) gray2[jx] = 0;
wire [NA-1:0] clksel;
always @(posedge refclk) begin
	for (jx=0; jx<NF; jx=jx+1) gray2[jx] <= gray1[jx];  // cross domains here
	gray3 <= gray2[clksel];  // multiplexing step
	gray4 <= gray3;  // probably useless pipeline stage
end

// Figure out how many unk_clk edges happened in the last refclk period
wire [gw-1:0] bin4 = gray4 ^ {1'b0, bin4[gw-1:1]}; // convert Gray to binary
reg [gw-1:0] bin5=0, diff5=0;
always @(posedge refclk) begin
	bin5 <= bin4;
	diff5 <= bin4 - bin5;
end

// Without squelch, would accumulate some crap when changing clocks.
// Integration time is therefore 2^rw-5 refclk cycles.
reg [uw-1:0] accum=0;
reg [rw-1:0] refcnt=0;
reg ref_carry=0, squelch=0;
reg [NA+cw-1:0] source_count=0, source_count_d=0;
always @(posedge refclk) begin
	{ref_carry, refcnt} <= refcnt + 1;
	if (ref_carry) source_count <= source_count + 1;
	accum <= (ref_carry | squelch) ? 0 : accum + diff5;
	if (ref_carry) squelch <= 1;
	if (refcnt[2]) squelch <= 0;
	source_count_d <= source_count;
end
assign clksel = source_count[NA-1:0];
assign source_state = source_count_d;

// Create a small RAM to hold the results
// Real-life FPGA RAM initializes to zero, but
// we're content with each entry starting at {uw{1'bx}} in simulation
reg [uw-1:0] freq_arr[0:NF-1], freq_r;
always @(posedge refclk) begin
	if (ref_carry) freq_arr[clksel] <= accum;
	freq_r <= freq_arr[addr];
end
assign frequency = freq_r;

endmodule
